# LabRepo Template Example

An LabRepo Template Example. Templates use the golang text/template package.

## Supported Variables
{{.Name}} - Repo Name as defined in labrepo.yaml
{{.URL}} - Repo URL as defined in labrepo.yaml or built from CI_PROJECT_PATH
{{.PublicKey.Filename}} - Filename of public key on server
{{.PublicKey.FingerPrint}} - GPG FingerPrint of signing key
{{.Packages}} - An array of built packages for repo
	{{.Name}} - Package Name
	{{.Version}} - Package Version
	{{.Arch}} - Package Arch
	{{.Filename}} - Package Filename on server
	{{.Description}} - Package Description

## Generating Key File
```sh
gpg --full-generate-key
gpg --export-secret-keys labrepo@example.com > signingkey.gpg
```
