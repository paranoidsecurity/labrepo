# LabRepo Signing Example 

A Signed Repo Example. Recommended For Unoffical Public Repositories

## Note
All Variables Can Be Environment Variables, SignPassPhrase SHOULD BE defined in an Environment Variable instead of commited in an git repo. Ideally you should push the public key you are going to use to a keyserver

## Generating Key File
```sh
gpg --full-generate-key
gpg --export-secret-keys labrepo@example.com > signingkey.gpg
```
