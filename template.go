package main

import (
	"encoding/json"
	"os"
	"path/filepath"
	"text/template"
)

func toJson(in interface{}) string {
	data, err := json.Marshal(in)
	if err != nil {
		panic(err)
	}
	return string(data)

}
func buildTemplates(config Config, pkgs []Desc) error {
	if len(config.Templates) == 0 {
		return nil
	}
	tmplFunc := template.FuncMap{
		"json": toJson,
	}
	data := struct {
		Name      string
		URL       string
		PublicKey struct {
			Filename    string
			FingerPrint string
		}
		Packages []Desc
	}{
		Name:     config.Name,
		URL:      config.Url,
		Packages: pkgs,
	}
	if keyId, err := config.GetKeyID(); err == nil {
		data.PublicKey = struct {
			Filename    string
			FingerPrint string
		}{
			Filename:    config.Name + ".gpg",
			FingerPrint: keyId,
		}
	}
	for t, o := range config.Templates {
		nt, err := template.New("labrepo").Funcs(tmplFunc).ParseFiles(t)
		if err != nil {
			return err
		}
		file, err := os.Create(filepath.Join(config.OutDir, o))
		if err != nil {
			return err
		}
		defer file.Close()
		if err := nt.ExecuteTemplate(file, t, data); err != nil {
			return err
		}
	}
	return nil
}
