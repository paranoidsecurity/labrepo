package main

import (
	"archive/tar"
	"crypto/md5"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"time"
)

func loadDB(config Config) *DB {
	db := &DB{
		config: config,
		desc:   map[string]Desc{},
	}
	f, err := os.Open(filepath.Join(config.OutDir, config.Name+".db"))
	if err != nil {
		return db
	}
	defer f.Close()
	tr := tar.NewReader(f)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			break
		}
		if hdr == nil {
			continue
		}
		switch hdr.Typeflag {
		case tar.TypeReg:
			data, _ := ioutil.ReadAll(tr)
			desc, _ := ParseDescContent(string(data))
			db.desc[desc.Name] = desc
		}
	}
	return db
}

type DB struct {
	config Config
	desc   map[string]Desc
}

func (db DB) ShouldBuild(pkg Package) bool {
	srcinfo, err := pkg.GetSRCINFO()
	if err != nil {
		return false
	}
	for _, pkg := range srcinfo.SplitPackages() {
		if d, ok := db.desc[pkg.Pkgname]; ok {
			if d.Version != srcinfo.Version() {
				return true
			}
		} else {
			return true
		}
	}
	return false
}

func (db DB) BuildDB() error {
	f, err := os.Create(filepath.Join(db.config.OutDir, db.config.Name+".db"))
	if err != nil {
		return err
	}
	defer f.Close()
	tw := tar.NewWriter(f)
	for _, desc := range db.desc {
		dir := &tar.Header{
			Typeflag: tar.TypeDir,
			Name:     fmt.Sprintf("%s-%s", desc.Name, desc.Version),
			Mode:     int64(0777),
			ModTime:  time.Now(),
		}
		if err := tw.WriteHeader(dir); err != nil {
			return err
		}
		content, err := WriteDescContent(desc)
		if err != nil {
			panic(err)
		}
		data := []byte(content)
		file := &tar.Header{
			Typeflag: tar.TypeReg,
			Name:     fmt.Sprintf("%s-%s/desc", desc.Name, desc.Version),
			Size:     int64(len(data)),
			Mode:     int64(0666),
			ModTime:  time.Now(),
		}
		if err := tw.WriteHeader(file); err != nil {
			return err
		}
		tw.Write(data)
	}
	return tw.Close()
}

func (db *DB) WritePackage(pkg Package) error {
	srcinfo, err := pkg.GetSRCINFO()
	if err != nil {
		return err
	}
	for _, pkg := range srcinfo.SplitPackages() {
		for _, arch := range pkg.Arch {
			if arch == "" {
				arch = "any"
			}
			file := filepath.Join(db.config.OutDir, pkg.Pkgname+"-"+srcinfo.Version()+"-"+arch+".pkg.tar.xz")
			if st, err := os.Stat(file); err == nil {
				desc := Desc{
					Url:         pkg.URL,
					Version:     srcinfo.Version(),
					Filename:    path.Base(file),
					Packager:    db.config.Maintainer,
					Base:        srcinfo.Pkgbase,
					CSize:       int(st.Size()),
					BuildDate:   time.Now(),
					Groups:      pkg.Groups,
					Provides:    []string{},
					Arch:        arch,
					Description: pkg.Pkgdesc,
					Name:        pkg.Pkgname,
					Depends:     []string{},
				}
				if pkg.Provides != nil {
					for _, prov := range pkg.Provides {
						desc.Provides = append(desc.Provides, prov.Value)
					}
				}
				if len(pkg.License) > 0 {
					desc.License = pkg.License[0]
				}
				if pubdata, err := ioutil.ReadFile(file + ".sig"); err == nil {
					desc.PgpSig = base64.StdEncoding.EncodeToString(pubdata)
				}
				f, err := os.Open(file)
				if err != nil {
					return err
				}
				defer f.Close()
				sha := sha256.New()
				md := md5.New()
				w := io.MultiWriter(sha, md)
				if _, err := io.Copy(w, f); err != nil {
					return err
				}
				desc.Sha256 = fmt.Sprintf("%x", sha.Sum(nil))
				desc.Md5 = fmt.Sprintf("%x", md.Sum(nil))
				if pkg.Depends != nil {
					for _, dep := range pkg.Depends {
						desc.Depends = append(desc.Depends, dep.Value)
					}
				}
				if old, ok := db.desc[desc.Name]; ok {
					os.Remove(filepath.Join(db.config.OutDir, old.Filename))
				}
				db.desc[desc.Name] = desc
			}
		}
	}
	return nil
}

func (db DB) SignDB() error {
	keyid, err := db.config.GetKeyID()
	if err != nil {
		return nil //No key
	}
	cmd := exec.Command("gpg", "--batch", "--yes", "-u", keyid, "-o", db.config.Name+".db.sig", "--detach-sign", db.config.Name+".db")
	cmd.Dir = db.config.OutDir
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func (db DB) GetPackages() []Desc {
	pkgs := []Desc{}
	for _, desc := range db.desc {
		pkgs = append(pkgs, desc)
	}
	return pkgs
}
