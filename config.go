package main

import (
	"fmt"
	"github.com/Morganamilo/go-srcinfo"
	"github.com/caarlos0/env"
	"gopkg.in/yaml.v2"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
)

type Package interface {
	Name() string
	PullPackages() error
	BuildPackages() error
	GetSRCINFO() (*srcinfo.Srcinfo, error)
	SetConfig(Config)
}

type Config struct {
	Name           string            `yaml:"RepoName" env:"REPONAME"`
	Maintainer     string            `yaml:"Maintainer" env:"MAINTAINER"`
	SignEntity     string            `yaml:"SignEntity" env:"SIGNENTITY"`
	SignPassPhrase string            `yaml:"SignPassPhrase" env:"SIGNPASSPHRASE"`
	Packages       []Package         `yaml:"Packages" env:"-"`
	DBCache        string            `yaml:"DBCache" env:"DBCACHE"`
	PKGCache       string            `yaml:"PKGCache" env:"PKGCACHE"`
	OutDir         string            `yaml:"OutDir" env:"OUTDIR"`
	Templates      map[string]string `yaml:"Templates" env:"-"`
	Url            string            `yaml:"Url" env:"URL"`
}

func (c Config) GetKeyID() (string, error) {
	signEntity, err := os.Open(c.SignEntity)
	if err != nil {
		return "", err
	}
	defer signEntity.Close()
	blck, err := armor.Decode(signEntity)
	if err != nil {
		return "", err
	}
	packReader := packet.NewReader(blck.Body)
	entity, err := openpgp.ReadEntity(packReader)
	if err != nil {
		return "", err
	}
	return entity.PrimaryKey.KeyIdString(), nil
}

func (c *Config) LoadConfig() error {
	c.Name = "labrepo"
	c.Maintainer = "LabRepo <labrepo@example.com>"
	c.DBCache = "cache/db"
	c.PKGCache = "cache/packages"
	c.OutDir = "public"
	if err := env.Parse(c); err != nil {
		return err
	}
	if c.Url == "" {
		if project := os.Getenv("CI_PROJECT_PATH"); project != "" {
			if splt := strings.SplitN(project, "/", 2); len(splt) == 2 {
				c.Url = fmt.Sprintf("https://%s.gitlab.io/%s/", splt[0], splt[1])
			}
		}
	}
	configpaths := []string{"/etc/labrepo.yaml"}
	if home := os.Getenv("HOME"); home != "" {
		configpaths = append(configpaths, filepath.Join(home, "labrepo.yaml"))
	}
	configpaths = append(configpaths, "labrepo.yaml")
	for _, file := range configpaths {
		f, err := os.Open(file)
		if err != nil {
			continue
		}
		defer f.Close()
		if err := yaml.NewDecoder(f).Decode(&c); err != nil {
			return err
		}
	}
	return nil
}

func (c *Config) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var str struct {
		Name           string            `yaml:"RepoName"`
		Maintainer     string            `yaml:"Maintainer"`
		SignEntity     string            `yaml:"SignEntity"`
		SignPassPhrase string            `yaml:"SignPassPhrase"`
		Packages       []GitPackage      `yaml:"Packages"`
		DBCache        string            `yaml:"DBCache"`
		PKGCache       string            `yaml:"PKGCache"`
		OutDir         string            `yaml:"OutDir"`
		Templates      map[string]string `yaml:"Templates"`
		Url            string            `yaml:"Url"`
	}
	if err := unmarshal(&str); err != nil {
		return err
	}
	c.Packages = []Package{}
	c.Templates = str.Templates
	if str.Name != "" {
		c.Name = str.Name
	}
	if str.Maintainer != "" {
		c.Maintainer = str.Maintainer
	}
	if str.SignEntity != "" {
		c.SignEntity = str.SignEntity
	}
	if str.SignPassPhrase != "" {
		c.SignPassPhrase = str.SignPassPhrase
	}
	if str.DBCache != "" {
		c.DBCache = str.DBCache
	}
	if str.PKGCache != "" {
		c.PKGCache = str.PKGCache
	}
	if str.OutDir != "" {
		c.OutDir = str.OutDir
	}
	if str.Url != "" {
		c.Url = str.Url
	}
	for i, _ := range str.Packages {
		str.Packages[i].SetConfig(*c)
		c.Packages = append(c.Packages, &str.Packages[i])
	}
	return nil
}
