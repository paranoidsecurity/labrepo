package main

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/Morganamilo/go-srcinfo"
	"gopkg.in/src-d/go-git.v4"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

type GitPackage struct {
	PKGName  string
	PKGBuild string

	config Config
}

func (gp GitPackage) Name() string {
	return gp.PKGName
}

func (gp *GitPackage) SetConfig(c Config) {
	gp.config = c
}

func (gp GitPackage) BuildPackages() error {
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	buf := bytes.NewBufferString("")
	args := []string{"-srf", "--config", filepath.Join(cwd, "makepkg.conf"), "--noconfirm", "--noprogressbar", "--needed", "-p", path.Base(gp.PKGBuild)}
	if keyid, err := gp.config.GetKeyID(); err == nil {
		args = append(args, "--key", keyid, "--sign")
	}
	cmd := exec.Command("makepkg", args...)
	cmd.Dir = filepath.Join(gp.GetOutDir(), path.Dir(gp.PKGBuild))
	cmd.Stdout = buf
	cmd.Stderr = buf
	if err := cmd.Run(); err != nil {
		f := bufio.NewWriter(os.Stdout)
		defer f.Flush()
		f.Write(buf.Bytes())
		return err
	}
	return nil
}

func (gp GitPackage) GetOutDir() string {
	gitpath := gp.PKGName
	if _, err := url.ParseRequestURI(gitpath); err != nil {
		gitpath = fmt.Sprintf("https://aur.archlinux.org/%s.git", gp.PKGName)
	}
	return filepath.Join(gp.config.PKGCache, strings.TrimSuffix(path.Base(gitpath), ".git"))
}

func (gp GitPackage) PullPackages() error {
	gitpath := gp.PKGName
	if _, err := url.ParseRequestURI(gitpath); err != nil {
		gitpath = fmt.Sprintf("https://aur.archlinux.org/%s.git", gp.PKGName)
	}
	os.MkdirAll(gp.config.PKGCache, 0777)
	outpath := gp.GetOutDir()
	if _, err := os.Stat(outpath); err == nil {
		r, err := git.PlainOpen(outpath)
		if err != nil {
			return err
		}
		w, err := r.Worktree()
		if err != nil {
			return err
		}
		opts := &git.PullOptions{
			RemoteName:    "origin",
			ReferenceName: "refs/heads/master",
			SingleBranch:  true,
			Force:         true,
		}
		if err := w.Pull(opts); err != nil {
			if err == git.NoErrAlreadyUpToDate {
				return nil
			}
			return err
		}
	} else {
		os.Mkdir(outpath, 0777)
		_, err := git.PlainClone(outpath, false, &git.CloneOptions{
			URL:           gitpath,
			ReferenceName: "refs/heads/master",
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (gp GitPackage) GetSRCINFO() (*srcinfo.Srcinfo, error) {
	src := bytes.NewBuffer([]byte(""))
	cmd := exec.Command("makepkg", "--printsrcinfo", "-p", path.Base(gp.PKGBuild))
	cmd.Dir = filepath.Join(gp.GetOutDir(), path.Dir(gp.PKGBuild))
	cmd.Stdout = src
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return nil, err
	}
	return srcinfo.Parse(string(src.Bytes()))
}

func (gp GitPackage) MarshalYAML() (interface{}, error) {
	if gp.PKGBuild == "PKGBUILD" {
		return gp.PKGName, nil
	}
	return struct {
		Name     string `yaml:"name"`
		PKGBuild string `yaml:"pkgbuild"`
	}{
		Name:     gp.PKGName,
		PKGBuild: gp.PKGBuild,
	}, nil
}

func (gp *GitPackage) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var val interface{}
	if err := unmarshal(&val); err != nil {
		return err
	}
	if s, ok := val.(string); ok {
		*gp = GitPackage{
			PKGName:  s,
			PKGBuild: "PKGBUILD",
		}
	} else {
		var str struct {
			Name     string `yaml:"name"`
			PKGBuild string `yaml:"pkgbuild"`
		}
		if err := unmarshal(&str); err != nil {
			return err
		}
		*gp = GitPackage{
			PKGName:  str.Name,
			PKGBuild: str.PKGBuild,
		}
	}
	return nil
}
