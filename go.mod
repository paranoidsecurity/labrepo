module gitlab.com/paranoidsecurity/labrepo

go 1.16

require (
	github.com/Morganamilo/go-srcinfo v1.0.0
	github.com/caarlos0/env v3.5.0+incompatible
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.4.0
)
