package main

import (
	"fmt"
	//"os"
	"regexp"
	"strings"
	"time"

	"bytes"
	"log"
	"reflect"
	"strconv"
)

type Desc struct {
	Url         string    `desc:"URL"`
	Version     string    `desc:"VERSION"`
	Filename    string    `desc:"FILENAME"`
	ISize       int       `desc:"ISIZE"`
	Packager    string    `desc:"PACKAGER"`
	PGPSig      string    `desc:"PGPSIG"`
	Sha256      string    `desc:"SHA256SUM"`
	Base        string    `desc:"BASE"`
	BuildDate   time.Time `desc:"BUILDDATE"`
	Groups      []string  `desc:"GROUPS"`
	Provides    []string  `desc:"PROVIDES"`
	Arch        string    `desc:"ARCH"`
	Md5         string    `desc:"MD5SUM"`
	CSize       int       `desc:"CSIZE"`
	Description string    `desc:"DESC"`
	PgpSig      string    `desc:"PGPSIG"`
	Name        string    `desc:"NAME"`
	License     string    `desc:"LICENSE"`
	Depends     []string  `desc:"DEPENDS"`
}

func ParseDescContent(content string) (Desc, error) {
	r := regexp.MustCompile("%(.*)%")
	splt := strings.Split(content, "\n\n")
	m := map[string][]string{}
	for _, s := range splt {
		spl := strings.Split(s, "\n")
		name := ""
		for _, l := range spl {
			match := r.FindStringSubmatch(l)
			if len(match) == 2 {
				name = match[1]
				m[name] = []string{}
			} else if m[name] != nil {
				m[name] = append(m[name], l)
			}
		}
	}
	d := Desc{}
	s := reflect.Indirect(reflect.ValueOf(&d))
	for i := 0; i < s.NumField(); i++ {
		vals := s.Field(i).Interface()
		name := s.Type().Field(i).Tag.Get("desc")
		for key, val := range m {
			if name == key {
				switch reflect.TypeOf(vals).String() {
				case "string":
					s.Field(i).SetString(val[0])
				case "[]string":
					if len(val) == 0 {
						continue
					}
					s.Field(i).Set(reflect.ValueOf(val))
				case "int":
					in, _ := strconv.Atoi(val[0])
					s.Field(i).SetInt(int64(in))
				case "time.Time":
					in, _ := strconv.Atoi(val[0])
					s.Field(i).Set(reflect.ValueOf(time.Unix(int64(in), 0)))
				default:
					log.Printf("Unsupported Type: %+v", reflect.TypeOf(vals))
				}
			}
		}
	}
	return d, nil
}

func WriteDescContent(d Desc) (string, error) {
	buf := bytes.NewBufferString("")
	s := reflect.Indirect(reflect.ValueOf(d))
	for i := 0; i < s.NumField(); i++ {
		vals := s.Field(i).Interface()
		name := s.Type().Field(i).Tag.Get("desc")
		if vals != nil {
			switch reflect.TypeOf(vals).String() {
			case "string":
				if vals.(string) != "" {
					fmt.Fprintf(buf, "%%%s%%\n", name)
					fmt.Fprintf(buf, "%s\n", vals.(string))
					fmt.Fprintln(buf)
				}
			case "[]string":
				if vals.([]string) != nil {
					if len(vals.([]string)) == 0 {
						continue
					}
					fmt.Fprintf(buf, "%%%s%%\n", name)
					for _, v := range vals.([]string) {
						fmt.Fprintf(buf, "%s\n", v)
					}
					fmt.Fprintln(buf)
				}
			case "int":
				if vals.(int) != 0 {
					fmt.Fprintf(buf, "%%%s%%\n", name)
					fmt.Fprintf(buf, "%d\n", vals.(int))
					fmt.Fprintln(buf)
				}
			case "time.Time":
				if vals.(time.Time).Unix() > 0 {
					fmt.Fprintf(buf, "%%%s%%\n", name)
					fmt.Fprintf(buf, "%d\n", vals.(time.Time).Unix())
					fmt.Fprintln(buf)
				}
			default:
				log.Printf("Unsupported Type: %+v", reflect.TypeOf(vals))
			}
		}
	}
	return buf.String(), nil
}
