package main

import (
	"log"

	"bufio"
	"fmt"
	"io"
	"os"
	"path"
	"strings"

	"os/exec"
	"path/filepath"
)

func main() {
	var config Config
	if err := config.LoadConfig(); err != nil {
		panic(err)
	}
	exe := path.Base(os.Args[0])
	if exe == "pinentry" {
		pinentry(config)
	} else {
		repo(config)
	}
}
func pinentry(config Config) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("OK Pleased to meet you")
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			return
		}
		splt := strings.SplitN(strings.TrimSpace(line), " ", 2)
		command := strings.ToLower(splt[0])
		if command == "getpin" {
			fmt.Printf("D %s\n", config.SignPassPhrase)
		}
		fmt.Println("OK")
		if command == "bye" {
			return
		}
	}
}
func setup(config Config) {
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatalf("Unable to get working directory: %s", err)
	}
	source, err := os.Open("/etc/makepkg.conf")
	if err != nil {
		log.Fatalf("Unable to open /etc/makepkg.conf: %s", err)
	}
	defer source.Close()
	destination, err := os.Create("makepkg.conf")
	if err != nil {
		log.Fatalf("Unable to open makepkg.conf: %s", err)
	}
	defer destination.Close()
	if _, err := io.Copy(destination, source); err != nil {
		log.Fatalf("Unable to copy: %s", err)
	}
	destination.Write([]byte("\nPKGDEST=" + filepath.Join(cwd, config.OutDir)))
	destination.Write([]byte("\nPACKAGER=\"" + config.Maintainer + "\""))

	cmd := exec.Command("gpg", "--import", config.SignEntity)
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		log.Fatalf("Unable to import key: %s", err)
	}
}
func repo(config Config) {
	if config.SignEntity != "" {
		config.Packages = append(config.Packages, KeyringPackage{
			GitPackage: &GitPackage{
				PKGName:  config.Name,
				PKGBuild: "PKGBUILD",

				config: config,
			},
		})
	}
	setup(config)
	db := loadDB(config)
	for _, pkg := range config.Packages {
		if err := pkg.PullPackages(); err != nil {
			log.Printf("Unable to get Package(%s): %s", pkg.Name(), err)
			continue
		}
		if db.ShouldBuild(pkg) {
			if err := pkg.BuildPackages(); err != nil {
				log.Printf("Unable to build Package(%s): %s", pkg.Name(), err)
				continue
			}
			if err := db.WritePackage(pkg); err != nil {
				log.Printf("Unable to write desc: %s", err)
				continue
			}
		} else {
			log.Printf("Package %s is up-to-date", pkg.Name())
		}
	}
	if err := db.BuildDB(); err != nil {
		log.Fatalf("Unable to build DB: %s", err)
	}
	if err := db.SignDB(); err != nil {
		log.Fatalf("Unable to sign DB: %s", err)
	}
	if err := buildTemplates(config, db.GetPackages()); err != nil {
		log.Fatalf("Unable to build templates: %s", err)
	}
}
