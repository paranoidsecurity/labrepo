# LabRepo 
[![pipeline status](https://gitlab.com/paranoidsecurity/labrepo/badges/master/pipeline.svg)](https://gitlab.com/paranoidsecurity/labrepo/commits/master)

Golang/Docker tool for creating archlinux repos on gitlab pages

---
* [Install](#install)
* [Configuration](#configuration)
* [License](#license)

---

## Install
```sh
docker pull registry.gitlab.com/paranoidsecurity/labrepo
```

## Configuration
This project uses viper so it supports any type viper does including environment variables. It searches for labrepo.* in one of the following directories: /etc/, ~/, and the current directory.
```yaml
---
Maintainer: LabRepo <labrepo@example.com>
SignEntity: gpgkey.gpg
SignPassPhrase: passphrase
RepoName: labrepo
Packages:
- ngrok
- jet
```

## License

Copyright 2019 James Kimble

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
