FROM golang:alpine as gobuild
RUN apk add --update git gcc libc-dev
ADD . /go/src/gitlab.com/paranoidsecurity/labrepo
WORKDIR /go/src/gitlab.com/paranoidsecurity/labrepo
RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o labrepo .

FROM archlinux:latest
RUN pacman -Syu --noconfirm git base-devel
COPY --from=gobuild /go/src/gitlab.com/paranoidsecurity/labrepo/labrepo /usr/local/bin/labrepo
RUN chmod +x /usr/local/bin/labrepo
RUN ln -sf /usr/local/bin/labrepo /usr/local/bin/pinentry
RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN useradd -m -g wheel labrepo
USER labrepo
