package main

import (
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
	"os"
	"path/filepath"
	"strconv"
	"text/template"
	"time"
)

type KeyringPackage struct {
	*GitPackage
}

func (kp KeyringPackage) PullPackages() error {
	pkgDir := filepath.Join(kp.GitPackage.config.PKGCache, kp.Name())
	os.MkdirAll(pkgDir, 0777)

	keyId, err := kp.GitPackage.config.GetKeyID()
	if err != nil {
		return err
	}
	keyCreationTime, err := getKeyCreation(kp.GitPackage.config.SignEntity)
	if err != nil {
		return err
	}
	version := strconv.Itoa(int(keyCreationTime.Unix()))
	if err := outPublicKey(kp.GitPackage.config.SignEntity, kp.GitPackage.config.OutDir, kp.Name()); err != nil {
		return err
	}
	if err := outPublicKey(kp.GitPackage.config.SignEntity, pkgDir, kp.Name()); err != nil {
		return err
	}
	if err := createInstall(pkgDir, kp.Name(), keyId); err != nil {
		return err
	}
	if err := createPKGBuild(pkgDir, kp.Name(), version); err != nil {
		return err
	}
	return nil
}

func getKeyCreation(privatekey string) (time.Time, error) {
	signEntity, err := os.Open(privatekey)
	if err != nil {
		return time.Now(), err
	}
	defer signEntity.Close()
	blck, err := armor.Decode(signEntity)
	if err != nil {
		return time.Now(), err
	}
	packReader := packet.NewReader(blck.Body)
	entity, err := openpgp.ReadEntity(packReader)
	if err != nil {
		return time.Now(), err
	}
	return entity.PrimaryKey.CreationTime, nil
}
func outPublicKey(privatekey, outdir, name string) error {
	signEntity, err := os.Open(privatekey)
	if err != nil {
		return err
	}
	defer signEntity.Close()
	blck, err := armor.Decode(signEntity)
	if err != nil {
		return err
	}
	packReader := packet.NewReader(blck.Body)
	entity, err := openpgp.ReadEntity(packReader)
	if err != nil {
		return err
	}
	out, err := os.Create(filepath.Join(outdir, name+".gpg"))
	if err != nil {
		return err
	}
	defer out.Close()
	w, err := armor.Encode(out, openpgp.PublicKeyType, nil)
	if err != nil {
		return err
	}
	defer w.Close()
	return entity.Serialize(w)
}

func createPKGBuild(out, name, version string) error {
	outFilepath := filepath.Join(out, "PKGBUILD")
	content := `
pkgname={{.RepoName}}-keyring
pkgver=v{{.Version}}
pkgrel=1
pkgdesc="{{.RepoName}} Keyring"
arch=('any')
install=${pkgname}.install
source=('{{.RepoName}}.gpg')
sha1sums=(SKIP)

package(){
	install -dm 7555 "${pkgdir}/usr/share/${pkgname}"

	install -Dm755 "{{.RepoName}}.gpg" "${pkgdir}/usr/share/${pkgname}/{{.RepoName}}.gpg"
}
	`
	return writeTemplatedFile(outFilepath, content, struct {
		RepoName string
		Version  string
	}{
		RepoName: name,
		Version:  version,
	})
}

func createInstall(out, name, keyId string) error {
	outFilepath := filepath.Join(out, name+"-keyring.install")
	content := `
post_install() {
  post_upgrade
}

post_upgrade() {
	usr/bin/pacman-key -a usr/share/{{.RepoName}}-keyring/{{.RepoName}}.gpg
	usr/bin/pacman-key --lsign-key {{.KeyId}}
}

post_remove() {
	usr/bin/pacman-key -d {{.KeyId}}
}
`
	return writeTemplatedFile(outFilepath, content, struct {
		RepoName string
		KeyId    string
	}{
		RepoName: name,
		KeyId:    keyId,
	})
}
func writeTemplatedFile(outFilepath, content string, data interface{}) error {
	os.Remove(outFilepath)
	file, err := os.Create(outFilepath)
	if err != nil {
		return err
	}
	defer file.Close()
	templ, err := template.New("file").Parse(content)
	if err != nil {
		return err
	}
	return templ.Execute(file, data)
}
